# 2020_assignment2_WineAdvisor

Secondo assignment - Processo e Sviluppo Software


### Membri del gruppo

Il progetto è stato realizzato da:

- Marchi Mattia 817587 @M-Marchi
- Stoppa Miguel 820506 @mig1214
- Tomasoni Lorenzo 829906 @lTomasoni


Fasi completate nella prima consegna:
- Background activity
- Individuazione degli stakeholders

Fasi completate nella seconda consegna
- Brainstoming
- Questionario
- Definizione Requisiti

